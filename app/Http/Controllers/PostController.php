<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'Judul' => 'required|unique:post',
            'Isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            "Judul" => $request["Judul"],
            "Isi" => $request["Isi"]
        ]);
        return redirect('/posts');
    }

    public function index()
    {
        $post = DB::table('pertanyaan')->get(); //SELECT * FROM post
        return view('post.index', compact('post'));
    }

    public function show($id)
    {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('post.show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'Judul' => 'required|unique:post',
            'Isi' => 'required',
        ]);

        $query = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'Judul' => $request["Judul"],
                'Isi' => $request["Isi"]
            ]);
        return redirect('/posts');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/posts');
    }
}
