<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkJawabanToJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jawaban', function (Blueprint $table) {
        $table->unsignedBigInteger('jawaban-tepat_id');
        $table->foreign('jawaban-tepat_id')->reference('id')->on('jawaban');
        $table->unsignedBigInteger('profil_id');
        $table->foreign('profil_id')->reference('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawaban', function (Blueprint $table) {
            $table->dropForeign(['jawaban-tepat_id']);
            $table->dropForeign(['profil_id']);
            $table->dropColumn(['jawaban-tepat_id']);
            $table->dropColumn(['profil_id']);
        });
    }
}
